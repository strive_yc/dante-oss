<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright (c) 2020-2030 ZHENGGENGWEI(码匠君)<herodotus@aliyun.com>
  ~
  ~ Dante Cloud licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ <http://www.apache.org/licenses/LICENSE-2.0>
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  ~ Dante Cloud 采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
  ~
  ~ 1.请不要删除和修改根目录下的LICENSE文件。
  ~ 2.请不要删除和修改 Dante Cloud 源码头部的版权声明。
  ~ 3.请保留源码和相关描述文件的项目出处，作者声明等。
  ~ 4.分发源码时候，请注明软件出处 <https://gitee.com/herodotus/dante-oss>
  ~ 5.在修改包名，模块名称，项目代码等时，请注明软件出处 <https://gitee.com/herodotus/dante-oss>
  ~ 6.若您的项目无法满足以上几点，可申请商业授权
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>cn.herodotus.oss</groupId>
    <artifactId>oss-bom</artifactId>
    <version>0.7.0</version>
    <packaging>pom</packaging>

    <description>Dante Cloud 生态产品 Dante OSS Bom</description>
    <name>oss-bom</name>

    <url>https://github.com/herodotus/dante-oss</url>

    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>https://www.apache.org/licenses/LICENSE-2.0</url>
        </license>
    </licenses>
    <scm>
        <url>https://github.com/herodotus-cloud/dante-oss</url>
        <connection>scm:git:git://github.com/herodotus-cloud/dante-oss.git</connection>
        <developerConnection>scm:git:ssh://github.com/herodotus-cloud/dante-oss.git</developerConnection>
        <tag>HEAD</tag>
    </scm>
    <organization>
        <name>Dromara</name>
        <url>https://github.com/dromara</url>
    </organization>
    <developers>
        <developer>
            <id>herodotus-cloud</id>
            <name>Herodotus</name>
            <email>herodotus@aliyun.com</email>
            <url>https://www.herodotus.cn</url>
            <timezone>+8</timezone>
        </developer>
    </developers>

    <!-- ${basedir} 项目根目录 -->
    <!-- ${project.build.directory} 构建目录，缺省为target -->
    <!-- ${project.build.outputDirectory} 构建过程输出目录，缺省为target/classes -->
    <!-- ${project.build.finalName} 产出物名称，缺省为${project.artifactId}-${revision} -->
    <!-- ${project.packaging} 打包类型，缺省为jar -->
    <!-- ${project.xxx} 当前pom文件的任意节点的内容 如project.artifactId -->
    <properties>
        <java.version>17</java.version>
        <resource.delimiter>@</resource.delimiter>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <maven-compiler-plugin.version>3.11.0</maven-compiler-plugin.version>
        <maven-javadoc-plugin.version>3.5.0</maven-javadoc-plugin.version>
        <maven-resources-plugin.version>3.3.1</maven-resources-plugin.version>
        <maven-source-plugin.version>3.3.0</maven-source-plugin.version>
        <maven-gpg-plugin.version>3.1.0</maven-gpg-plugin.version>

        <lombok.version>1.18.28</lombok.version>
        <mapstruct-processor.version>1.5.5.Final</mapstruct-processor.version>

        <spring-boot-dependencies.version>3.1.0</spring-boot-dependencies.version>
        <dante-bom.version>3.1.0.8</dante-bom.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot-dependencies.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>cn.herodotus.engine</groupId>
                <artifactId>dependencies</artifactId>
                <version>${dante-bom.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!-- oss -->
            <dependency>
                <groupId>cn.herodotus.oss</groupId>
                <artifactId>minio-core</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>cn.herodotus.oss</groupId>
                <artifactId>minio-sdk-logic</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>cn.herodotus.oss</groupId>
                <artifactId>minio-sdk-rest</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>cn.herodotus.oss</groupId>
                <artifactId>minio-sdk-scenario</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>cn.herodotus.oss</groupId>
                <artifactId>minio-spring-boot-starter</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>cn.herodotus.oss</groupId>
                <artifactId>oss-spring-boot-starter</artifactId>
                <version>${project.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*</include>
                </includes>
                <filtering>true</filtering>
            </resource>
        </resources>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven-compiler-plugin.version}</version>
                    <configuration>
                        <source>${java.version}</source>
                        <target>${java.version}</target>
                        <encoding>${project.build.sourceEncoding}</encoding>
                        <showWarnings>true</showWarnings>
                        <annotationProcessorPaths>
                            <path>
                                <groupId>org.projectlombok</groupId>
                                <artifactId>lombok</artifactId>
                                <version>${lombok.version}</version>
                            </path>
                            <path>
                                <groupId>org.mapstruct</groupId>
                                <artifactId>mapstruct-processor</artifactId>
                                <version>${mapstruct-processor.version}</version>
                            </path>
                            <path>
                                <groupId>org.springframework.boot</groupId>
                                <artifactId>spring-boot-configuration-processor</artifactId>
                                <version>${spring-boot-dependencies.version}</version>
                            </path>
                        </annotationProcessorPaths>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>${maven-resources-plugin.version}</version>
                    <configuration>
                        <encoding>UTF-8</encoding>
                        <propertiesEncoding>UTF-8</propertiesEncoding>
                        <addDefaultExcludes>false</addDefaultExcludes>
                        <delimiters>
                            <delimiter>${resource.delimiter}</delimiter>
                        </delimiters>
                        <useDefaultDelimiters>false</useDefaultDelimiters>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>${maven-source-plugin.version}</version>
                    <executions>
                        <execution>
                            <id>attach-sources</id>
                            <phase>package</phase>
                            <goals>
                                <goal>jar-no-fork</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>${maven-javadoc-plugin.version}</version>
                    <configuration>
                        <show>package</show>
                        <tags>
                            <tag>
                                <name>date</name>
                            </tag>
                        </tags>
                    </configuration>
                    <executions>
                        <execution>
                            <id>attach-javadocs</id>
                            <phase>package</phase>
                            <goals>
                                <goal>jar</goal>
                            </goals>
                            <configuration>
                                <doclint>none</doclint>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-gpg-plugin</artifactId>
                    <version>${maven-gpg-plugin.version}</version>
                    <executions>
                        <execution>
                            <phase>verify</phase>
                            <goals>
                                <goal>sign</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <!--单独为Dependencies模块发布做处理-->
    <profiles>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                    </plugin>
                </plugins>
            </build>

            <distributionManagement>
                <snapshotRepository>
                    <id>sonatype-nexus-snapshots</id>
                    <name>Sonatype Nexus Snapshots</name>
                    <url>https://s01.oss.sonatype.org/content/repositories/snapshots</url>
                </snapshotRepository>
                <repository>
                    <id>sonatype-nexus-release</id>
                    <name>Nexus Release Repository</name>
                    <url>https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/</url>
                </repository>
            </distributionManagement>
        </profile>
    </profiles>

    <repositories>
        <repository>
            <id>spring-milestones</id>
            <name>Spring Milestones</name>
            <url>https://repo.spring.io/milestone</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>spring-snapshots</id>
            <name>Spring Snapshots</name>
            <url>https://repo.spring.io/snapshot</url>
            <releases>
                <enabled>false</enabled>
            </releases>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>spring-milestones</id>
            <name>Spring Milestones</name>
            <url>https://repo.spring.io/milestone</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </pluginRepository>
        <pluginRepository>
            <id>spring-snapshots</id>
            <name>Spring Snapshots</name>
            <url>https://repo.spring.io/snapshot</url>
            <releases>
                <enabled>false</enabled>
            </releases>
        </pluginRepository>
    </pluginRepositories>

</project>